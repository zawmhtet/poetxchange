from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'market.views.landing_home', name='home'),
    url(r'^home/books$','market.views.home_books', name='home_books'),
    url(r'^home/stuffs/$','stuffs.views.home_stuffs',name='home_stuffs'),
    url(r'^user/signup/valid_code=(?P<user_validCode>\d+)/$','market.views.activation', name='activate'),
    url(r'^login', 'market.views.login_user', name='login'),
    url(r'^logout', 'market.views.logout_user', name='logout'),
    url(r'^profile/(?P<pro_id>\d+)/$', 'market.views.profile', name='profile'),
    url(r'^add_book', 'market.views.add_book', name='add_book'),
    url(r'^book/(?P<book_id>\d+)/$',  'market.views.book_perma', name='book_perma'),
    url(r'^book_request/(?P<book_id>\d+)/?$', 'market.views.book_request', name='book_request'),
    url(r'^book_unrequest/(?P<book_id>\d+)/?$', 'market.views.book_unrequest', name='book_unrequest'),
##    url(r'^test','market.views.the_test',name='the_test'),
    url(r'^edit$','market.views.edit_settings',name='edit'),
    url(r'^12(?P<book_id>\d+)/?$','market.views.edit_book',name='edit_book'),
    url(r'^1pxcdb4(?P<book_id>\d+)/?$','market.views.delete_book',name='delete_book'),
    url(r'^infrd','market.views.invite_friends',name='invite_friends'),
    url(r'^add_stuff','stuffs.views.add_stuff', name='add_stuff'),
    url(r'^stuff_request/(?P<stuff_id>\d+)/?$','stuffs.views.stuff_request', name='stuff_request'),
    url(r'^stuff_unrequest/(?P<stuff_id>\d+)/?$','stuffs.views.stuff_unrequest', name='stuff_unrequest'),
    url(r'^stuff/edit/(?P<stuff_id>\d+)/?$','stuffs.views.edit_stuff', name='edit_stuff'),
    url(r'^1sxcdb4(?P<stuff_id>\d+)/?$','stuffs.views.delete_stuff', name='delete_stuff'),
##    url(r'^about','market.views.about',name='about'),
##    url(r'^team','market.views.team',name='team'),
##    url(r'^internship','market.views.internship',name='internship'),
##    url(r'^chat','chat.views.chatroom',name='chat'),
##    url(r'^ajaxtest','chat.views.ajaxtest', name='ajaxtest'),
##    url(r'^test','market.views.book_test'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
