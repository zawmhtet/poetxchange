$(document).ready(function(){
	$("#inner-right-togglebar").click(function(){
		$("#left").animate({left:"-95%"},1000);
		$("#right").animate({left:"0"},1000);
		$("#inner-left-togglebar").show(1000);
		$("#inner-right-togglebar").hide(1000);
	});

	$("#inner-left-togglebar").click(function(){
		$("#right").animate({left:"95%"},1000);
		$("#left").animate({left:"0"},1000);
		$("#inner-right-togglebar").show(1000);
		$("#inner-left-togglebar").hide(1000);
	});
	
});


function showPage(selector){
	if (curPage!=""){
		$(curPage).animate({left:"50%"});
		$(curPage).hide(500);
		$(curPage).animate({left:"0"});
	}
	$(selector).delay(600);
	$(selector).show(1000);
	curPage = selector;
}