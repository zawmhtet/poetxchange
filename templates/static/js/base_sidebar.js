$(document).ready(function(){
	$("#click2").hide();
	$("div.addbook").hide();
	$("div.addstuff").hide();

	$("#click").click(function(){togglesidebar("#click");})

	$("p.addbook").click(function(){
		if ($("div.addbook").is(":visible")==true){
			togglesidebar();
		}
		else{
			if ($("#click2").is(":visible")==true){
				$("div.addstuff").hide(500);
				$("div.addbook").show(500);
			}
			else{
				togglesidebar("div.addbook");
			}
		}
	})

	$("p.addstuff").click(function(){
		if ($("div.addstuff").is(":visible")==true){
			togglesidebar();
		}
		else{
			if ($("#click2").is(":visible")==true){
				$("div.addbook").hide(500);
				$("div.addstuff").show(500);
			}
			else{
				togglesidebar("div.addstuff");
			}
		}
	})
});

function togglesidebar(xdiv){
	if ($("#click2").is(":visible")==true){
		$("#content").animate({left:"90px"});
		$("#sidebar_dashboard").animate({width:"0px"});
		$("#sidebar").animate({width:"90px"});
		$("#click2").hide();
		$("div.addbook").hide(500);
		$("div.addstuff").hide(500);
		$(".sidebarfooter").hide();
	}
	else
	{
		$("#content").animate({left:"500px"});
		$("#sidebar_dashboard").animate({width:"410px"});
		$("#sidebar").animate({width:"500px"});
		$("#click2").show();
		$(xdiv).show(500);
		$(".sidebarfooter").show(600);
	}
};
