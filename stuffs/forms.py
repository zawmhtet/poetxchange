from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from stuffs.models import *

class StuffForm(forms.Form):
	stuff_name = forms.CharField(label=(u'stuff_name'),required=True,error_messages={'required':"Don't leave this blank"}, widget = forms.TextInput(attrs={'placeholder':'Item name'}))
	stuff_description = forms.CharField( label=(u'stuff_description'), widget = forms.Textarea(attrs={'placeholder':'More details about your item and how people can contact you','rows':4}))
	stuff_price = forms.DecimalField( label=(u'stuff_price'), required=True, widget = forms.TextInput(attrs={'size':3,'maxlength':6,'placeholder':'99.99'}))

class StuffImgForm(forms.Form):
	stuff_imgField = forms.ImageField(required = True)
