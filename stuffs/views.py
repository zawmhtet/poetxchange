from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from stuffs.forms import *
from market.forms import * 
from market.models import userAccount
from django.template import RequestContext
from django.core.mail import EmailMessage

# Create your views here.
def home_stuffs(request):
    if request.user.is_authenticated():
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        all_stuffs = Stuff.objects.all().order_by('-stuff_pub_date')
        bform = BookForm()
        simgform = StuffImgForm()
        sform = StuffForm()
        bimgform = BookImgForm()
        return render_to_response('home_stuffs.html',RequestContext(request,{'stuffs':all_stuffs, 'usr_acc':usr_acc, 'bform':bform,'sform':sform, 'simgform':simgform, 'bimgform': bimgform}))

def add_stuff(request):
	if request.user.is_authenticated() and request.method == 'POST':
		sform = StuffForm(request.POST)
		stuffimgform = StuffImgForm(request.POST, request.FILES)
		if sform.is_valid() and stuffimgform.is_valid():
			usr_acc = userAccount.objects.get(user_id = request.user.id)
			stuff = Stuff.objects.create(
				stuff_owner = usr_acc,
				stuff_name = sform.cleaned_data['stuff_name'],
				stuff_price= sform.cleaned_data['stuff_price'],
				stuff_description = sform.cleaned_data['stuff_description'],
				)
			stuff.stuff_img = stuffimgform.cleaned_data['stuff_imgField']
			stuff.save()
			send_email = EmailMessage( "Your item, " + stuff.stuff_name + " has been uploaded for sale", "Your item is now up for sale. Interested users will 'like' your stuff and you will be able to see their contact info in your profile.\nStuff details: " + stuff.stuff_name + "\nDescription: " + stuff.stuff_description + "\nPrice: $" + str(stuff.stuff_price),to = [stuff.stuff_owner.email])
			send_email.send()
			return HttpResponseRedirect('/')
		else:
			return render_to_response('add_stuff.html', RequestContext(request,{'sform':sform,'stuffimgform':stuffimgform}))
	else:
		return HttpResponseRedirect('/')

def edit_stuff(request,stuff_id):
    if request.user.is_authenticated():
        stuff = Stuff.objects.get(id = stuff_id)
        if stuff.is_owner(request.user.id):
            if request.method == 'POST':
                form = StuffForm(request.POST)
                simgform = StuffImgForm(request.POST,request.FILES)
                simgform.fields['stuff_imgField'].required = False
                if form.is_valid() and simgform.is_valid():
                    simg = simgform.cleaned_data['stuff_imgField']
                    if simg != None:
                        stuff.stuff_img.delete()
                        stuff.stuff_img = simg
                    stuff.stuff_name = form.cleaned_data['stuff_name']
                    stuff.stuff_description = form.cleaned_data['stuff_description']
                    stuff.stuff_price = form.cleaned_data['stuff_price']
                    stuff.save()
                    return HttpResponseRedirect('/profile/'+str(stuff.stuff_owner.user.id))
                else:
                    form = StuffForm(initial={'stuff_name':stuff.stuff_name,'stuff_description':stuff.stuff_description,'stuff_price':stuff.stuff_price})
                    bform = BookForm()
                    sform = StuffForm()
                    bimgform = BookImgForm()
                    simgform = StuffImgForm()
                    return render_to_response('edit_stuff.html', RequestContext(request,{'form':form, 'bimgform':bimgform,'stuff':stuff,'sform':sform,'bform':bform, 'simgform': simgform}))
            else:
                form = StuffForm(initial={'stuff_name':stuff.stuff_name,'stuff_description':stuff.stuff_description,'stuff_price':stuff.stuff_price})
                bform = BookForm()
                sform = StuffForm()
                simgform = StuffImgForm()
                bimgform = BookImgForm()
                return render_to_response('edit_stuff.html', RequestContext(request,{'form':form,'simgform':simgform,'stuff':stuff,'bform':bform,'sform':sform, 'bimgform': bimgform}))
        else:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def delete_stuff(request,stuff_id):
        if request.user.is_authenticated() and request.method == 'POST':
                stuff = Stuff.objects.get(id=stuff_id)
                if stuff.is_owner(request.user.id):
                        stuff.stuff_img.delete()
                        stuff.delete()
                        return HttpResponseRedirect('profile/'+str(stuff.stuff_owner.user.id))
                else:
                        return HttpResponseRedirect('/')
        else:
                return HttpResponseRedirect('/')
        
def stuff_request(request,stuff_id):
    if request.method == "POST":
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        stuff = Stuff.objects.get(id = stuff_id)
        if SnU.objects.filter(related_stuff = stuff):
            snu = SnU.objects.get(related_stuff = stuff)
            snu.interested_users.add(usr_acc)
        else:
            snu = SnU.objects.create(related_stuff = stuff)
            snu.save()
            snu.interested_users.add(usr_acc)
            subj = usr_acc.fullname() + " is interested in " + stuff.stuff_name
            send_email = EmailMessage( subj, str(usr_acc.fullname()) + ' is interested in buying your stuff, ' + stuff.stuff_name + '. Go to your profile to see contact info.', to = [stuff.stuff_owner.email])
            send_email.send()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def stuff_unrequest(request,stuff_id):
    if request.method == "POST":
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        stuff = Stuff.objects.get(id = stuff_id)
        if SnU.objects.filter(related_stuff = stuff):
            snu = SnU.objects.get(related_stuff = stuff)
            snu.interested_users.remove(usr_acc)
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
