from django.db import models
from django.contrib.auth.models import User
import datetime, os
from PIL import Image

# Create your models here.
def get_stuffpic_path(instance,filename):
    return os.path.join('static/s39s/',str(instance.stuff_owner.id),str(instance.id),"sks.jpg")

class Stuff(models.Model):
	stuff_owner = models.ForeignKey("market.userAccount")
	stuff_name = models.CharField(max_length = 100)
	stuff_img = models.ImageField(upload_to = get_stuffpic_path , blank = True, null=True)
	stuff_price = models.DecimalField(max_digits = 8, decimal_places = 2)
	stuff_description = models.CharField(max_length = 300)
	stuff_pub_date = models.DateTimeField('date_published')

	def __unicode__(self):
		return self.stuff_owner.fullname() + "'s" + self.stuff_name

        def is_owner(self,user_id):
                return self.stuff_owner.user.id == user_id

        def save(self):
                super(Book,self).save()
                if self.book_img:
                        image = Image.open(self.book_img)
                        (width, height) = image.size
                        size = (120,120)
                        image = image.resize(size, Image.ANTIALIAS)
                        image.save(self.book_img.path)

        @property
        def snu(self):
                l = []
                try:
                        su = SnU.objects.get(related_stuff = self)
                        for t in su.interested_users.all():
                                l.append(t)
                except:
                        l = None
                return l

        def save(self,*args, **kwargs):
                self.stuff_pub_date = datetime.datetime.today()
                super(Stuff,self).save(*args, **kwargs)

class SnU(models.Model):
    """This model links between Stuff and how many users are interested in it."""
    related_stuff = models.ForeignKey("Stuff")
    interested_users = models.ManyToManyField("market.userAccount")

    def __unicode__(self):
        return self.related_stuff.stuff_name
