from django.contrib import admin
from stuffs.models import *
# Register your models here.
class stuffAdmin(admin.ModelAdmin):
    list_display = ('id','stuff_owner', 'stuff_description', 'stuff_price', 'stuff_img')

class snuAdmin(admin.ModelAdmin):
    list_display = ('related_stuff',)

admin.site.register(Stuff, stuffAdmin)    
admin.site.register(SnU, snuAdmin)
