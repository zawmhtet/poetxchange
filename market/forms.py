from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from market.models import *


DEPTCHOICES = (
        ('',''),
        ('ANTH', 'ANTH'),
        ('ART', 'ART'),
        ('BIOL', 'BIOL'),
        ('BSAD', 'BSAD'),
        ('CHDV', 'CHDV'),
        ('CHEM', 'CHEM'),
        ('CHIN', 'CHIN'),
        ('COSC', 'COSC'),
        ('ECON', 'ECON'),
        ('EDUC', 'EDUC'),
        ('ENGL', 'ENGL'),
        ('ENST', 'ENST'),
        ('ENVS', 'ENVS'),
        ('FILM', 'FILM'),
        ('FREN', 'FREN'),
        ('GCS', 'GCS'),
        ('GWS', 'GWS'),
        ('HIST', 'HIST'),
        ('INTD', 'INTD'),
        ('JAPN', 'JAPN'),
        ('KNS', 'KNS'),
        ('MATH', 'MATH'),
        ('MUS', 'MUS'),
        ('OTHR', 'OTHR'),
        ('PHIL', 'PHIL'),
        ('PHYS', 'PHYS'),
        ('PSYC', 'PSYC'),
        ('REL', 'REL'),
        ('SOC', 'SOC'),
        ('SOWK', 'SOWK'),
        ('SPAN', 'SPAN'),
        ('THEA', 'THEA'),
        ('WSP', 'WSP'),
)

CONDITIONS = ( 
    ('New','New'),
    ('Used','Used'),
    ('Good','Good'),
    ('Worn','Worn')
)

class InitRegForm(forms.Form):
        email_uname = forms.CharField(max_length=15,
                                      widget = forms.TextInput(
                                              attrs = {
                                                      'class':'pemail',
                                                      'size':3,
                                                      'maxlength':15,
                                                      'placeholder':'poet'
                                                      }
                                              )
                                      )
        first_name = forms.CharField(max_length=15,
                                     widget = forms.TextInput(
                                             attrs = {
                                                     'size':8,
                                                     'maxlength':15,
                                                     'placeholder':'First Name'
                                                     }
                                             )
                                     )
        last_name = forms.CharField(max_length=15,
                                    widget = forms.TextInput(
                                            attrs = {
                                                    'size':8,
                                                    'maxlength':15,
                                                    'placeholder':'Last Name'
                                                    }
                                            )
                                    )
        password = forms.CharField(label=(u'password'),
                                   widget=forms.PasswordInput(
                                           render_value=False,
                                           attrs={'placeholder':'Password'}
                                           )
                                   )
        username = forms.CharField(max_length = 10,
                                   widget = forms.TextInput(
                                           attrs = {
                                                   'size':10,
                                                   'maxlength':10,
                                                   'placeholder':'Username'
                                                   }
                                           )
                                   )
        email = forms.EmailField(label=(u'Email Address'),
                                 required = False,
                                 widget=forms.TextInput(
                                         attrs={'placeholder':'Email'}
                                         )
                                 )
#        ph_num = forms.CharField( max_length = 10, required = False, widget = forms.TextInput(attrs={'type':'tel',
#                                                                                                    'maxlength':10,
#                                                                                                    'size':10,
#                                                                                                    'placeholder':'Phone - ##########',
#                                                                                                    }
#                                                                                             )
#                                  )

        def clean_email_uname(self):
                email_uname = self.cleaned_data.get('email_uname')
                if User.objects.filter(email = email_uname + '@poets.whittier.edu'):
                        raise forms.ValidationError("This email address is already in use.")
                return email_uname

        def clean_username(self):
                usrname = self.cleaned_data.get('username')
                if userAccount.objects.filter(username = usrname):
                        raise forms.ValidationError("This username is already taken")
                return usrname

#        def clean_ph_num(self):
#                try:
#                        tel = int(self.cleaned_data.get('ph_num'))
#                except:
#                        raise forms.ValidationError("Phone number is not valid (No space or dash)")
##                if len(tel) < 10:
##                        raise forms.ValidationError("Phone number is not valid")
#                return tel

class ActivationForm(forms.Form):
        username = forms.CharField(max_length = 8,
                                   widget = forms.TextInput(
                                           attrs = {
                                                   'size':10,
                                                   'maxlength':10,
                                                   'placeholder':'Choose username'
                                                   }
                                           )
                                   )
        password        = forms.CharField(label=(u'password'), widget=forms.PasswordInput(render_value=False, attrs={'placeholder':'Password'}))
        email		= forms.EmailField(label=(u'Email Address'), widget=forms.TextInput(attrs={'placeholder':'Leave blank to use College\'s email'}))

        def clean_username(self):
                username = self.cleaned_data['username']
                try:
                        User.objects.get(username=username)
                except User.DoesNotExist:
                        return username
                raise forms.ValidationError('Username is taken')

class LoginForm(forms.Form):
        username = forms.CharField(label=(u'Username'),widget=forms.TextInput(attrs={'placeholder':'Username'}))
        password = forms.CharField(label=(u'Password'), widget=forms.PasswordInput(render_value=False,attrs={'placeholder':'Password'}))

class BookForm(forms.Form):
        title = forms.CharField( label=(u'title'), widget = forms.TextInput(attrs={'placeholder':'Title of the book'}))
        description = forms.CharField( label=(u'description'), widget = forms.Textarea(attrs={'placeholder':'More details about your book and how to contact you','rows':4}))
        price = forms.DecimalField( label=(u'price'), required=True, widget = forms.TextInput(attrs={'size':3,'maxlength':6,'placeholder':'99.99'}))
        related_major = forms.ChoiceField( choices = DEPTCHOICES, required=True, error_messages = {'required':'Please specify the related major of this book.'})
        condition = forms.ChoiceField( choices = CONDITIONS, required=True)

        def clean_title(self):
                title = self.cleaned_data['title'].strip()
                if title:
                        return title
                else:
                        raise forms.ValidationError("What\'s the title of the book?")

class editForm(forms.Form):
        first_name = forms.CharField(max_length=15,
                                     widget = forms.TextInput(
                                             attrs = {
                                                     'size':8,
                                                     'maxlength':15,
                                                     'placeholder':'First Name'
                                                     }
                                             )
                                     )
        last_name = forms.CharField(max_length=15,
                                    widget = forms.TextInput(
                                            attrs = {
                                                    'size':8,
                                                    'maxlength':15,
                                                    'placeholder':'Last Name'
                                                    }
                                            )
                                    )
        password = forms.CharField(label=(u'password'),
                                   required = False,
                                   widget=forms.PasswordInput(
                                           render_value=False,
                                           attrs={'placeholder':'Change Password'}
                                           )
                                   )
        username = forms.CharField(max_length = 8,
                                   widget = forms.TextInput(
                                           attrs = {
                                                   'size':10,
                                                   'maxlength':10,
                                                   'placeholder':'Choose username'
                                                   }
                                           )
                                   )
        email = forms.EmailField(label=(u'Email Address'),
                                 required = False,
                                 widget=forms.TextInput(
                                         attrs={'placeholder':'Leave blank to use College\'s email'}
                                         )
                                 )
#        ph_num = forms.CharField( max_length = 10, required = True, widget = forms.TextInput(attrs={'type':'tel',
#                                                                                                    'maxlength':10,
#                                                                                                    'size':10
#                                                                                                    }
#                                                                                             )
#                                  )

#        def clean_ph_num(self):
#                try:
#                        tel = int(self.cleaned_data.get('ph_num'))
#                except:
#                        raise forms.ValidationError("Phone number is not valid")
##                if len(tel) < 10:
##                        raise forms.ValidationError("Phone number is not valid")
#                return tel

class edit_BookForm(forms.Form):
        title = forms.CharField( label=(u'title'), widget = forms.TextInput(attrs={'placeholder':'Title of the book'}))
        description = forms.CharField( label=(u'description'), widget = forms.Textarea(attrs={'placeholder':'More details about your book and how to contact you','rows':4,'cols':50}))
        price = forms.DecimalField( label=(u'price'), widget = forms.TextInput(attrs={'size':3,'maxlength':6,'placeholder':'Set the price'}))
        related_major = forms.ChoiceField( choices = DEPTCHOICES, required=True, error_messages = {'required':'Please specify the related major of this book.'})
        condition = forms.ChoiceField( choices = CONDITIONS, required=True)

        def clean_title(self):
                title = self.cleaned_data['title'].strip()
                if title:
                        return title
                else:
                        raise forms.ValidationError("What\'s the title of the book?")

class ImgForm(forms.Form):
        imgField = forms.ImageField(required = False)

class BookImgForm(forms.Form):
        imgField = forms.ImageField(required = True)

class invitefriendform(forms.Form):
        email_uname = forms.CharField(max_length=15,
                                      widget = forms.TextInput(
                                              attrs = {
                                                      'class':'pemail',
                                                      'size':3,
                                                      'maxlength':15,
                                                      'placeholder':'Email'
                                                      }
                                              )
                                      )
