from django.contrib import admin
from market.models import *

# Register your models here.
class userAccountAdmin(admin.ModelAdmin):
    list_display = ('id','user','username','first_name','last_name','email')

class RegValidatorAdmin(admin.ModelAdmin):
    list_display = ('valid_code','username','first_name','last_name','email')

class BookAdmin(admin.ModelAdmin):
    list_display = ('id','owner','title','related_major','price','condition','pub_date')

class BnUAdmin(admin.ModelAdmin):
    list_display = ('related_book',)

admin.site.register(userAccount, userAccountAdmin)
admin.site.register(RegValidator, RegValidatorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(BnU, BnUAdmin)
