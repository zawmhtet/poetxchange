from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from market.forms import *
from stuffs.forms import *
from django.template import RequestContext
from market.functions import *
from django.contrib.auth import authenticate, login, logout
from django.core.mail import EmailMessage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime

# Create your views here.
def home(request):
    cur_time = datetime.now()
    if request.user.is_authenticated():
        dum_list = []
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        all_books = Book.objects.all().order_by('-pub_date')[:5]
        all_stuffs = Stuff.objects.all().order_by('-stuff_pub_date')[:5]
        dum_list += all_books
        dum_list += all_stuffs
        dum_list = sorted(dum_list, key=sort_with_pub_date, reverse=True)
        paginator = Paginator(dum_list, 15) #Show 5 items per page
        page = request.GET.get('page')
        try:
            item_lists = paginator.page(page)
        except PageNotAnInteger:
            item_lists = paginator.page(1)
        except EmptyPage:
            item_lists = paginator.page(paginator.num_pages)

        bform = BookForm()
        bimgform = BookImgForm()
	sform = StuffForm()
    simgform = StuffImgForm()
    return render_to_response('home.html',RequestContext(request,{'books':all_books, 'stuffs':all_stuffs,
                                'usr_acc':usr_acc, 'bform':bform,'sform':sform, 'bimgform':bimgform,
                                'cur_time':datetime.now(), 'item_lists': item_lists, 'simgform': simgform}))

def sort_with_pub_date(obj):
    try:
        return obj.pub_date
    except:
        return obj.stuff_pub_date

def home_books(request):
    if request.user.is_authenticated():
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        all_books = Book.objects.all().order_by('-pub_date')
        bform = BookForm()
        bimgform = BookImgForm()
        sform = StuffForm()
        simgform = StuffImgForm()
        return render_to_response('home_books.html',RequestContext(request,{'books':all_books, 'usr_acc':usr_acc, 'bform':bform,'sform':sform, 'bimgform':bimgform, 'simgform': simgform}))
    
def login_user(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/profile')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            account = authenticate(username = form.cleaned_data['username'], password = form.cleaned_data['password'])
            if account is not None:
                login(request, account)
                return HttpResponseRedirect('/')
            else:
                return render_to_response('login.html', RequestContext(request,{'form':form}))
        else:
            return render_to_response('login.html',RequestContext(request,{'form':form}))
    else:
        form = LoginForm()
        return render_to_response('login.html', RequestContext(request,{'form':form}))

def logout_user(request):
    logout(request)
    form=InitRegForm()
    return HttpResponseRedirect('/')


def profile(request,pro_id):
    if request.user.is_authenticated():
        usr_acc = userAccount.objects.get(user_id = pro_id)
        u_acc = userAccount.objects.get(user_id = request.user.id)
        books = Book.objects.filter(owner = usr_acc)
        stuffs = Stuff.objects.filter(stuff_owner = usr_acc)
        if request.user.id == usr_acc.user.id: #Is whoever viewing the profile the real owner?
            bkr_list = book_requests(usr_acc) #List of books user have requested
            stuff_list = stuff_requests(usr_acc)
        else:
            bkr_list = None
            stuff_list = None
        bform = BookForm()
        bimgform = BookImgForm()
        sform = StuffForm()
        simgform = StuffImgForm()
        inviteform = invitefriendform()
        return render_to_response('profile.html', RequestContext(request,{'u_acc':u_acc,'usr_acc':usr_acc,'books':books,'stuffs':stuffs, 'bkrs':bkr_list, 'stuff_list':stuff_list, 'bform':bform,'sform':sform, 'bimgform':bimgform, 'inviteform':inviteform, 'simgform': simgform}))
    else:
        return HttpResponseRedirect('/admin')


def add_book(request):
    if request.user.is_authenticated() and request.method == 'POST':
        form = BookForm(request.POST)
        bimgform = BookImgForm(request.POST,request.FILES)
        if form.is_valid() and bimgform.is_valid():
	        usr_acc = userAccount.objects.get(user_id = request.user.id)
                book = Book.objects.create(
                    owner = usr_acc,
                    title = form.cleaned_data['title'],
                    description = form.cleaned_data['description'],
                    related_major = form.cleaned_data['related_major'],
                    price = form.cleaned_data['price'],
                    condition = form.cleaned_data['condition'],
                    )
                book.book_img = bimgform.cleaned_data['imgField']
                book.save()
                send_email = EmailMessage( "Your book " + book.title + " has been uploaded for sale", "Your book is now up for sale. Interested users will 'like' your book and you will be able to see their contact info in your profile. Book details:\nTitle: " + book.title + "\nDescription: " + book.description + "\nPrice: $" + str(book.price) + "\nCondition: " + book.condition, to = [book.owner.email])
                send_email.send()
                return HttpResponseRedirect('/')
	else:
                return render_to_response('add_book.html', RequestContext(request,{'form':form, 'bimgform':bimgform}))
##        else:
##            form = BookForm()
##            bimgform = ImgForm()
##            return render_to_response('add_book.html', RequestContext(request,{'form':form,'bimgform':bimgform}))
    else:
        return HttpResponseRedirect('/')

def book_perma(request, book_id):
    if request.user.is_authenticated():
        book = Book.objects.get(id = book_id)
        return render_to_response('book.html', RequestContext(request, {'book':book}))
    else:
        return HttpResponseRedirect('/')


def book_request(request,book_id):
    if request.method == "POST":
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        book = Book.objects.get(id = book_id)
        if BnU.objects.filter(related_book = book):
            bnu = BnU.objects.get(related_book = book)
            bnu.interested_users.add(usr_acc)
        else:
            bnu = BnU.objects.create(related_book = book)
            bnu.save()
            bnu.interested_users.add(usr_acc)
            subj = usr_acc.fullname() + " requested to buy " + book.title
            send_email = EmailMessage( subj, str(usr_acc.fullname()) + ' is interested in buying your book, ' + book.title + '. Go to your profile to see contact info.', to = [book.owner.email])
            send_email.send()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def book_unrequest(request,book_id):
    if request.method == "POST":
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        book = Book.objects.get(id = book_id)
        if BnU.objects.filter(related_book = book):
            bnu = BnU.objects.get(related_book = book)
            bnu.interested_users.remove(usr_acc)
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def the_test(request):
    bnu = BnU.objects.all()
    return render_to_response('test.html', RequestContext(request,{'bu':bnu}))

def landing_home(request):
    if request.user.is_authenticated():
        return home(request)
    if request.method == "POST":
        signup_form = InitRegForm(request.POST)
        if signup_form.is_valid():
            return register_user(request,signup_form)
        else:
            signin_form = LoginForm()
            return render_to_response('landing_page.html', RequestContext(request,{'signup_form':signup_form, 'signin_form':signin_form}))
    else:
        signup_form = InitRegForm()
        signin_form = LoginForm()
        return render_to_response('landing_page.html', RequestContext(request,{'signup_form':signup_form, 'signin_form':signin_form}))

def register_user(request,form):
    oldVal = RegValidator.objects.filter(poet_email = form.cleaned_data['email_uname']+"@poets.whittier.edu")
    if oldVal:
        oldVal.delete()
##        print ("It's deleted")
    email_poet = form.cleaned_data['email_uname']+ '@poets.whittier.edu'
    email_prefer = form.cleaned_data['email']
    if email_prefer == None or email_prefer == "":
        email_prefer = email_poet
    regVal = RegValidator.objects.create(
                username = form.cleaned_data['username'],
                first_name = form.cleaned_data['first_name'],
                last_name = form.cleaned_data['last_name'],
                valid_code = valCodeGenerator(),
                poet_email = email_poet,
                email = email_prefer,
                password = form.cleaned_data['password'],
#                ph_num = form.cleaned_data['ph_num'])
                ph_num = 0000000000)
    regVal.save()
    host = request.get_host()
    regUrl = request.get_host() + '/user/signup' + ('/valid_code=%s' % regVal.valid_code)
    send_email = EmailMessage('Activation Link', 'Here is the link to activate your account ' + regUrl, to = [email_poet])
    send_email.send()
    return render_to_response('success.html', RequestContext(request,{}))
##    return render_to_response('success.html', RequestContext(request,{'act_link':regVal.valid_code}))

def activation(request,user_validCode):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    if RegValidator.objects.filter(valid_code = user_validCode):
            activate_acc = RegValidator.objects.get(valid_code = user_validCode)
            user = User.objects.create_user(username = activate_acc.username,password = activate_acc.password, email = activate_acc.poet_email)
            user.save()
            userAcc = userAccount.objects.create(user = user,username = activate_acc.username, first_name = activate_acc.first_name, last_name = activate_acc.last_name, email = activate_acc.email, ph_num = activate_acc.ph_num)
            userAcc.save()
            pw = activate_acc.password
            account = authenticate(username = user.username , password = pw)
            activate_acc.delete()
            if account is not None:
                login(request,account)
                send_email = EmailMessage('Activation Success', 'Your PoetXchange account has been activated. Thank you for signing up.\nHere is your login info.\nUsername: ' + user.username + '\nPassword: ' + pw, to = [user.email])
                send_email.send()
                return HttpResponseRedirect('/')
            else:
                return render_to_response('error.html', RequestContext(request,{}))
    else:
        return HttpResponseRedirect('/')

def edit_settings(request):
    if request.user.is_authenticated():
        usr_acc = userAccount.objects.get(user_id = request.user.id)
        if request.method == 'POST':
            form = editForm(request.POST)
            #imgform = ImgForm(request.POST,request.FILES)
            if form.is_valid(): #and imgform.is_valid():
                usr_acc.first_name = form.cleaned_data['first_name']
                usr_acc.last_name = form.cleaned_data['last_name']
                usr_acc.username = form.cleaned_data['username']
                request.user.username = usr_acc.username
                #usr_acc.ph_num = form.cleaned_data['ph_num']
                usr_acc.ph_num = 0000000000
                email_prefer = form.cleaned_data['email']
                pw = form.cleaned_data['password']
                #propic = imgform.cleaned_data['imgField']
                if email_prefer == None:
                    email_prefer = request.user.email
                if pw != None and pw != "":
                    request.user.set_password(pw)
                #if propic != None:
                #    usr_acc.user_img.delete()
                #    usr_acc.user_img = request.FILES['imgField']
                usr_acc.email = email_prefer
                usr_acc.save()
                request.user.save()
                return HttpResponseRedirect('profile/'+str(usr_acc.user.id))
            else:
                #imgform = ImgForm()
                form = editForm(initial={'first_name':usr_acc.first_name, 'last_name':usr_acc.last_name, 'password':request.user.password, 'username':usr_acc.username, 'password':'123456', 'email':usr_acc.email, 'ph_num':usr_acc.ph_num})
                return render_to_response('edit.html',RequestContext(request,{'form':form, 'usr_acc':usr_acc,})) #'imgform':imgform}))
        else:
            #imgform = ImgForm()
            form = editForm(initial={'first_name':usr_acc.first_name, 'last_name':usr_acc.last_name, 'password':request.user.password, 'username':usr_acc.username, 'password':'123456', 'email':usr_acc.email, 'ph_num':usr_acc.ph_num})
            return render_to_response('edit.html',RequestContext(request,{'form':form, 'usr_acc':usr_acc,})) #'imgform':imgform}))
    else:
        return HttpResponseRedirect('/')

def edit_book(request,book_id):
    if request.user.is_authenticated():
        book = Book.objects.get(id = book_id)
        if book.is_owner(request.user.id):
            if request.method == 'POST':
                form = edit_BookForm(request.POST)
                bimgform = BookImgForm(request.POST,request.FILES)
                bimgform.fields['imgField'].required = False
                if form.is_valid() and bimgform.is_valid():
                    bimg = bimgform.cleaned_data['imgField']
                    if bimg != None:
                        book.book_img.delete()
                        book.book_img = bimg
                    book.title = form.cleaned_data['title']
                    book.description = form.cleaned_data['description']
                    book.price = form.cleaned_data['price']
                    book.related_major = form.cleaned_data['related_major']
                    book.condition = form.cleaned_data['condition']
                    book.save()
                    return HttpResponseRedirect('profile/'+str(book.owner.user.id))
                else:
                    form = edit_BookForm(initial={'title':book.title,'description':book.description,'price':book.price,'related_major':book.related_major,'condition':book.condition})
                    bform = BookForm()
                    sform = StuffForm()
                    bimgform = BookImgForm()
                    simgform = StuffImgForm()
                    return render_to_response('edit_book.html', RequestContext(request,{'form':form, 'bimgform':bimgform,'book':book,'sform':sform,'bform':bform, 'simgform': simgform}))
            else:
                form = edit_BookForm(initial={'title':book.title,'description':book.description,'price':book.price,'related_major':book.related_major,'condition':book.condition})
                bform = BookForm()
		sform = StuffForm()
                bimgform = BookImgForm()
                simgform = StuffImgForm()
                return render_to_response('edit_book.html', RequestContext(request,{'form':form,'bimgform':bimgform,'book':book,'bform':bform,'sform':sform, 'simgform': simgform}))
        else:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def delete_book(request,book_id):
    if request.user.is_authenticated() and request.method == 'POST':
        book = Book.objects.get(id=book_id)
        if book.is_owner(request.user.id):
            book.book_img.delete()
            book.delete()
            return HttpResponseRedirect('profile/'+str(book.owner.user.id))
        else:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def invite_friends(request):
    if request.user.is_authenticated() and request.method == 'POST':
        inviteform = invitefriendform(request.POST)
        if inviteform.is_valid():
            usr_acc = userAccount.objects.get(user_id = request.user.id)
            email_poet = inviteform.cleaned_data['email_uname']+ '@poets.whittier.edu'
            if User.objects.filter(email = email_poet):
                print ("Your friend is already registered")
            send_email = EmailMessage('Invitation from your friend ' + str(usr_acc.fullname()), 'Hey, sign up at poetxchange.com.', to = [email_poet])
            send_email.send()
            return HttpResponseRedirect('profile/' + str(request.user.id))
        else:
            return HttpResponseRedirect('profile/' + str(request.user.id))
    else:
        return HttpResponseRedirect('/')
