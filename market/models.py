from django.db import models
from django.contrib.auth.models import User
import datetime, os
from django.conf import settings
from PIL import Image

# Create your models here.
DEPTCHOICES = (
    ('ANTH', 'ANTH'),
    ('ART', 'ART'),
    ('BIOL', 'BIOL'),
    ('BSAD', 'BSAD'),
    ('CHDV', 'CHDV'),
    ('CHEM', 'CHEM'),
    ('CHIN', 'CHIN'),
    ('COSC', 'COSC'),
    ('ECON', 'ECON'),
    ('EDUC', 'EDUC'),
    ('ENGL', 'ENGL'),
    ('ENST', 'ENST'),
    ('ENVS', 'ENVS'),
    ('FILM', 'FILM'),
    ('FREN', 'FREN'),
    ('GCS', 'GCS'),
    ('GWS', 'GWS'),
    ('HIST', 'HIST'),
    ('INTD', 'INTD'),
    ('JAPN', 'JAPN'),
    ('KNS', 'KNS'),
    ('MATH', 'MATH'),
    ('MUS', 'MUS'),
    ('OTHR', 'OTHR'),
    ('PHIL', 'PHIL'),
    ('PHYS', 'PHYS'),
    ('PSYC', 'PSYC'),
    ('REL', 'REL'),
    ('SOC', 'SOC'),
    ('SOWK', 'SOWK'),
    ('SPAN', 'SPAN'),
    ('THEA', 'THEA'),
    ('WSP', 'WSP'),
)

CONDITIONS = ( 
    ('New','New'),
    ('Used','Used'),
    ('Good','Good'),
    ('Worn','Worn')
)

def get_profilepic_path(instance,filename):
    return os.path.join('static/imgs/pp/' + str(instance.id) + "/pp.jpg")

def get_bookpic_path(instance,filename):
    return os.path.join('static/b39b/',str(instance.owner.id),str(instance.id),"bkb.jpg")

class userAccount(models.Model):
    """
    This is the model for user account
    """
    user = models.OneToOneField(User, unique=True)
    user_img = models.ImageField(upload_to=get_profilepic_path, blank=True, null=True)
##    user_img_thumbnail = models.ImageField(upload_to=get_profilepic_path(pp-thumb), default=os.path.join('static','icos','anonymous.png'), blank=True, null=True)
    username = models.CharField(max_length=20)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(max_length=50)
    ph_num = models.IntegerField(max_length = 10)
    def __unicode__(self):
        return self.username

    def fullname(self):
        return self.first_name + " " + self.last_name

    def save(self, *args, **kwargs):
        """
        Profile picture (img file) has to be saved with a method
        """
        super(userAccount,self).save(*args, **kwargs)
#        if self.user_img:
#            image = Image.open(self.user_img)
#            (width, height) = image.size
#            size = (160,160)
#            image = image.resize(size, Image.ANTIALIAS)
#            image.save(self.user_img.path)

class RegValidator(models.Model):
    valid_code = models.CharField(max_length=10, unique=True)
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=50)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(max_length=50)
    poet_email = models.EmailField(max_length=50)
    ph_num = models.IntegerField(max_length = 10)

class Book(models.Model):
    owner = models.ForeignKey("userAccount")
    book_img = models.ImageField(upload_to=get_bookpic_path, blank=True, null=True)
    title = models.CharField(max_length = 50)
    description = models.CharField(max_length = 300)
    related_major = models.CharField(max_length = 4, choices = DEPTCHOICES)
##    price = models.IntegerField(max_length = 4)
    price = models.DecimalField(max_digits=8, decimal_places = 2)
    condition = models.CharField(max_length = 5, choices = CONDITIONS)
##    ISBN = models.CharField(max_length = 13, blank=True, null=True)
    pub_date = models.DateTimeField('date published')
##    for_sale = models.BooleanField()
##    interested_users = models.ManyToManyField(userAccount)
    
    def __unicode__(self):
        return self.title + "Posted by: " + self.owner.username

    def is_owner(self,user_id):
        return self.owner.user.id == user_id
    
    def save(self):
        super(Book,self).save()
        if self.book_img:
            image = Image.open(self.book_img)
            (width, height) = image.size
            size = (120,120)
            image = image.resize(size, Image.ANTIALIAS)
            image.save(self.book_img.path)
        
    @property
    def bnu(self):
        l = []
        try:
            bu = BnU.objects.get(related_book = self)
            for t in bu.interested_users.all():
                l.append(t)
        except:
            l = None
        return l

    def save(self,*args,**kwargs):
        self.pub_date = datetime.datetime.today()
        super(Book,self).save(*args, **kwargs)

class BnU(models.Model):
    """
    This model links between a Book and how many users are interested in it.
    'Likes'
    """
    related_book = models.ForeignKey("Book")
    interested_users = models.ManyToManyField("market.userAccount")

    def __unicode__(self):
        return self.related_book.title

class activity(models.Model):
    """This model tracks the activity of users"""
    actype = models.CharField(max_length = 4, choices = DEPTCHOICES)
    user_a = models.ForeignKey("userAccount")
    act_description = models.CharField(max_length = 30)
    
