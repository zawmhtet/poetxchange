from django.contrib import admin
from chat.models import *
# Register your models here.

class chatheadAdmin(admin.ModelAdmin):
    list_display = ('chat_book','chat_user')

class chatMsgAdmin(admin.ModelAdmin):
    list_display = ('chat_head','msg','owner_boo')

admin.site.register(chathead, chatheadAdmin)
admin.site.register(chatMsg, chatMsgAdmin)
