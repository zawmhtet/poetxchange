from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from chat.models import *

class chatform(forms.Form):
    chat_msg = forms.CharField(label=(u'chat'), widget = forms.Textarea(attrs={'placeholder':'CHAT!'}))
