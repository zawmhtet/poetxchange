from django.db import models
from django.contrib.auth.models import User
import datetime, os
# Create your models here.

class chatMsg(models.Model):
    chat_head = models.ForeignKey("chathead")
    msg = models.TextField(blank = True)
    owner_boo = models.BooleanField()

    def __unicode__(self):
        return self.msg

class chathead(models.Model):
    chat_book = models.ForeignKey("market.Book")
    chat_user = models.ForeignKey("market.userAccount")
    
