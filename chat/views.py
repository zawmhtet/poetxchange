from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from chat.forms import *
from django.template import RequestContext
from market.functions import *
from django.contrib.auth import authenticate, login, logout
from django.core.mail import EmailMessage

# Create your views here.
def chatroom(request):
    if request.method == 'POST':
        form = chatform(request.POST)
        if form.is_valid():
            msg = form.cleaned_data['chat_msg']
            chead = chathead.objects.get(id=1)
            boo = False
            chatMsg.objects.create(chat_head = chead, msg = msg, owner_boo=boo).save()
            return HttpResponseRedirect('/chatroom')
    else:
        form = chatform()
        conversation = chatMsg.objects.all()
        return render_to_response('chatroom.html',RequestContext(request,{'form':form,'convers':conversation}))

def ajaxtest(request):
##    print(request.POST['text'])
    if request.method == "POST":
        msg = request.POST['text']
        chead = chathead.objects.get(id=1)
        boo = False
        chatMsg.objects.create(chat_head = chead, msg = msg, owner_boo=boo).save()
    l = chatMsg.objects.all()
    return HttpResponse(l[len(l)-1])
